FROM golang:1.16.3-alpine3.13 AS builder

RUN mkdir /app

ADD . /app

WORKDIR /app
RUN go mod download

RUN GOOS=linux GOARCH=amd64 go build -o app .


FROM alpine:latest 
RUN apk --no-cache add ca-certificates
RUN mkdir /app
WORKDIR /app
COPY --from=builder /app .

ENTRYPOINT [ "./app" ]