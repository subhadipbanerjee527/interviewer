all: build push
build:
	@echo "Building image..."
	docker build -t leo191/interviewer:1.0 . 

push:
	@echo "pushing to docker hub"
	docker push leo191/interviewer:1.0
