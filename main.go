package main

import (
	// "fmt"

	// "context"
	"database/sql"
	"log"
	// "time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/template/html"
	// "go.mongodb.org/mongo-driver/bson"
	// "go.mongodb.org/mongo-driver/mongo"
	// "go.mongodb.org/mongo-driver/mongo/options"
	// "go.mongodb.org/mongo-driver/mongo/readpref"
    _ "github.com/go-sql-driver/mysql"
)

const (  
    username = "root"
    password = "life"
    hostname = "127.0.0.1:3306"
    dbname   = "test"
)

type Todo struct{
    Id          int     `json:"id"`
    Name        string  `json:"name"`
    Completed   bool    `json:"completed"`
}


type formdata struct{
    Login string `json:"email"`
    Password string `json:"password"`
}



func main() {
    app := fiber.New(fiber.Config{
        Views: html.New("./views", ".html"),
    })
    app.Use(logger.New())
    app.Static("/static", "./views/css")
    app.Get("/", func (c *fiber.Ctx) error {
        if c.Request.  
        return c.Render("login", fiber.Map{
            "title" : "Interviewer",
            "loginissue" : "",
        })
    })
    app.Post("/auth/login", Login)
    app.Post("/home", func (c *fiber.Ctx) error {
        var fdata formdata
        if err := c.BodyParser(&fdata); err != nil{
            log.Fatal(err)
            c.Status(fiber.StatusBadRequest).JSON(fiber.Map{ "erro" : err })
        }
        return c.Status(fiber.StatusOK).JSON(&fdata)
    })
    log.Fatal(app.Listen(":3000"))
}

func ConnectDB() *sql.DB {
    uri := username + ":" + password + "@tcp(" + hostname +")/" + dbname
    db, err := sql.Open("mysql", uri)
    if err != nil {
        log.Fatalln("Issue with connecting db", err)
    }
    log.Println("DB connected successfully")
    return db
}

func Login(c *fiber.Ctx) error {
    db := ConnectDB()
    defer db.Close()
    var fdata formdata
    var name string
    if err := c.BodyParser(&fdata); err != nil{
        log.Fatal(err)
        return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{ "erro" : err })
    }
    log.Printf("Logging as %s\n", fdata.Login)
    rows, err := db.Query("select name from users where email = ? and password = ?", fdata.Login, fdata.Password)
    if err != nil {
        log.Println("Error searching user", err)
    }
    defer rows.Close()
    for rows.Next() {
        err := rows.Scan(&name)
        if err != nil {
			log.Fatal(err)
		}
    }
    if name == "" {
        return c.Redirect("/", 404)

    }
    return c.SendString(name)

}


// func CreateTodos(c *fiber.Ctx) error {
//     ctx, cancel := context.WithTimeout(context.Background(), 10* time.Second)
//     defer cancel()
//     client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://mongodb:27017"))
//     defer func() {
//         if err = client.Disconnect(ctx); err != nil {
//             panic(err)
//         }
//     }()
//     client.Ping(ctx, readpref.Primary())
//     var body request
//     if err := c.BodyParser(&body); err != nil{
//         log.Fatal(err)
//         c.Status(fiber.StatusBadRequest).JSON(fiber.Map{ "erro" : err })
//     }
//     todo := Todo{
//         Id: len(todos) + 1,
//         Name: body.Name,
//         Completed: false,
//     }
//     todos = append(todos, todo)
//     collection := client.Database("testing").Collection("TODO")
//     res, err := collection.InsertOne(ctx, bson.D{{"id", 1}, {"name", body.Name}, {"completed", false}})
//     if err != nil {
//         log.Fatal(err)
//     }
//     log.Println(res.InsertedID)
//     return c.Status(fiber.StatusCreated).JSON(todos)
// }

// func GetTodos(c *fiber.Ctx) error {
//     return c.Status(fiber.StatusOK).JSON(todos)
// }